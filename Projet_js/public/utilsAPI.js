"use strict";

//post the score
function postScore(nvScore){
    //call the POST /users API to post the current score
    $.ajax({
                type: "post",
                url: "/scores",
                data: {pseudo:$("#pseudo").val(),score:nvScore},
                dataType: "json",
                success: function (response) {
                    
					console.log("Pseudo" + $("#pseudo").val());
                },
                error: function name(err, status, message) {
                    console.log("Error :",status,"messsage:", message);
                }
			});
}

//get all the scores and print in a list
function getScores()
{
    //call the GET /users API to get all the registered users
    $.ajax({
        type: "get",
        url: "/scores",
        dataType: "json",
        success: function (response) {
            let resp_json = response;
            if (resp_json.success) {
                $("#scores").text("");
                //write some dynamic html content within the div
                let list = document.createElement("ul");
                let users_array = response.data ;
                users_array.sort((a, b) => (a.score < b.score) ? 1 : -1);
                console.log(users_array);
                list.className="list-group";
                for (let i = 0; i < users_array.length; i++) {
                    var item = document.createElement("li");
                    item.className = "list-group-item";
                    item.innerText = users_array[i].pseudo + " Scores : " + users_array[i].score ;
        
                    list.appendChild(item);
                }
                $("#scores").append(list);  
            }
            else
            {
                $("#scores").text(JSON.stringify(response.error)) ;  
            }
              
        }
    }); 
}






