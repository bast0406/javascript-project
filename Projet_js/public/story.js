
//setup for a new story
const startStory = () => {
	$("#intro").show();
	$("#pseudoF").show();
	$("#introStory").show();
	$("#question2").hide();
	$("#question21").hide();
	$("#question22").hide();
	$("#question211").hide();
	$("#question212").hide();
	$("#question221").hide();
	$("#question222").hide();
	$("#question2121").hide();
	$("#question2122").hide();
	$("#question21211").hide();
	$("#question21212").hide();
	$("#question212121").hide();
	$("#question212120").hide();
	$("#question212122").hide();
	$("#question21221").hide();
	$("#question21222").hide();
	$("#question2211").hide();
	$("#question2212").hide();
	$("#question2222").hide();
	$("#question22121").hide();
	$("#question22122").hide();
	$("#question221222").hide();
	$("#question221221").hide();
	$("#game").hide();
	$("#scores").hide();
	$("#reset").hide();

};

//hide everything
const hideAll = () =>{
	$("#intro").hide();
	$("#pseudoF").hide();
	$("#introStory").hide();
	$("#question2").hide();
	$("#question21").hide();
	$("#question22").hide();
	$("#question211").hide();
	$("#question212").hide();
	$("#question221").hide();
	$("#question222").hide();
	$("#question2121").hide();
	$("#question2122").hide();
	$("#question21211").hide();
	$("#question21212").hide();
	$("#question212121").hide();
	$("#question212120").hide();
	$("#question212122").hide();
	$("#question21221").hide();
	$("#question21222").hide();
	$("#question2211").hide();
	$("#question2212").hide();
	$("#question2222").hide();
	$("#question22121").hide();
	$("#question22122").hide();
	$("#question221222").hide();
	$("#question221221").hide();
	$("#startHs").hide();
	$("#game").hide();
	$("#scores").hide();
	$("#reset").hide();

	console.log("hide");

}

//launch our game
const affficherJeu = () =>{
	var script = document.createElement('script');
	script.src = "./labGame.js";
	document.getElementsByTagName('head')[0].appendChild(script);

}



$(document).ready(function() {
	startStory();

	$("#startHs").click( function(e){
		e.preventDefault();
		if ( $("#pseudo").get(0).checkValidity() ) {
		postScore(0);
		hideAll();
		$("#question2").show(); 
		}
	});

	$("#btnVoirScore").click( function(){
		hideAll();
		$("#scores").show();

	});

	$("#choice21").click( function(){
		hideAll();
		$("#question21").show();
		postScore(15);

	});
	$("#choice22").click( function(){
		hideAll();
        postScore(10);
		$("#question22").show();


	});
	$("#choice211").click( function(){
		postScore(10);
		hideAll();
		$("#question211").show();


	});
	$("#choice212").click( function(){
		postScore(15);
		hideAll();
		$("#question212").show();


	});
	$("#choice221").click( function(){
		hideAll();
        postScore(15);
		$("#question221").show();

	});
	$("#choice222").click( function(){
		hideAll();
        postScore(10);
		$("#question222").show();

	});
	$("#choice2121").click( function(){
		hideAll();
        postScore(10);
		$("#question211").show();
	});
	
	$("#choice2122").click( function(){
		hideAll();
        postScore(15);
		$("#question2122").show();


	});
	$("#choice2211").click( function(){
		hideAll();
		 postScore(0);
		$("#question2211").show();
		$("#reset").show();
		getScores();
		$("#scores").show();


	});
	$("#choice2212").click( function(){
		hideAll();
		 postScore(15);
		$("#question2212").show();

	});
	$("#choice2221").click( function(){
		hideAll();
        postScore(-40);

		$("#question21").show();

	});
	$("#choice2222").click( function(){
		hideAll();
		 postScore(10);
		$("#question2222").show();

	});
	$("#choice21221").click( function(){
		hideAll();
		postScore(15);
		$("#question21221").show();

	});
	$("#choice21222").click( function(){
		hideAll();
		postScore(50);
		$("#question21222").show();
	});
	$("#choice212210").click( function(){
		hideAll();
		postScore(-30);
		$("#question211").show();


	});
	$("#choice21211").click( function(){
		hideAll();
		postScore(0);
		$("#question21211").show();
		$("#reset").show();
		getScores();
		$("#scores").show();

	});
	$("#choice21212").click( function(){
		hideAll();
		postScore(15);
		$("#question21212").show();
	});
	$("#choice22122").click( function(){
		hideAll();
		postScore(5);
		$("#question22122").show();

	});
	$("#choice22121").click( function(){
		hideAll();
		postScore(5);
		$("#question22121").show();

	});
	$("#choice212121").click( function(){
		hideAll();
		postScore(20);
		$("#question212121").show();


	});
	$("#choice212120").click( function(){
		hideAll();
		postScore(0);
		$("#question212120").show();
		$("#reset").show();
		getScores();
		$("#scores").show();


	});
	$("#choice212122").click( function(){
		hideAll();
		postScore(-5);
		$("#question212122").show();
		$("#question21212").show();
		$("#choice212122").hide();


	});
	$("#choice212220").click( function(){
		hideAll();
		postScore(10);
		affficherJeu();
	});
	$("#choice221221").click( function(){
		hideAll();
		postScore(20);
		$("#question221221").show();

	});
	$("#choice221211").click( function(){
		hideAll();
		postScore(20);
		$("#question221221").show();

	});
	$("#choice221222").click( function(){
		hideAll();
		postScore(-5);
		$("#question221222").show();

	});
	$("#choice221212").click( function(){
		hideAll();
		postScore(-5);

		$("#question221222").show();

	});
	$("#choice2121210").click( function(){
		hideAll();
		postScore(15);
		affficherJeu();
	});
	$("#choice221221b").click( function(){
		hideAll();
		postScore(-5);
		$("#question221221").show();

	});
	$("#choice2212210").click( function(){
		hideAll();
		postScore(20);
		affficherJeu();

	});
	$("#choice22220").click( function(){
		hideAll();
		postScore(-45);
		$("#question21").show();

	});
	$("#reset").click( function(){
		location.reload();
	});




});




