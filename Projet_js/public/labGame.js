

var config = {
    type: Phaser.WEBGL,
    width: 800,
    height: 600,
    parent: 'game-container',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 } // Top down game, so no gravity
          }
    },
    scene: {
        preload: preload,
        create: create,
        update:update
    }
};

var game = new Phaser.Game(config);
var map;
var player;
var cursors;
var layer;
var tileset;
var endGame;
var timeEvent;
var timeText;
var counter;
var speed;
var spotlight;
var ice = false;

function preload ()
{
    this.load.tilemapTiledJSON('map1', 'assets/layoutLab.json');
    this.load.tilemapTiledJSON('map2', 'assets/layoutLab2.json');
    this.load.tilemapTiledJSON('map3', 'assets/layoutLab3.json');
    this.load.tilemapTiledJSON('map4', 'assets/layoutLab4.json');
    this.load.image('tiles', 'assets/gridtiles.png');
    this.load.image('scores', 'assets/scores.png');
    this.load.image('mask', 'assets/mask1.png');
    this.load.spritesheet('dude', 'assets/dude.png', { frameWidth: 32, frameHeight: 48 });
    this.load.image('fin', 'assets/esprit.jpg');
}

function create ()
{
    //init the variable
    endGame= false;
    counter = 60;
    speed = 160;
    var mapn = Phaser.Math.RND.pick(['map1', 'map2','map3','map4']);

    //load map, player and tiles needed
    var container = this.add.container();
    map = this.make.tilemap({ key: mapn });
    tileset = map.addTilesetImage('tiles');
    layerBelow = map.createStaticLayer('base', tileset);
    layerUP = map.createStaticLayer('wall', tileset);
    player = this.physics.add.sprite(32+16, 32+16, 'dude');
    player.setScale(0.6);
    var container = this.add.container();
    container.add([ layerBelow, layerUP,player ]);
    spotlight = this.make.sprite({
        x: 400,
        y: 300,
        key: 'mask',
        add: false
    });

    //init fog of war
    container.mask = new Phaser.Display.Masks.BitmapMask(this, spotlight);

    //init physics
    this.physics.add.existing(player);
    layerUP.setCollisionBetween(1,10);
    this.physics.add.collider(player, layerUP);

    //flavour text
    if(mapn==='map1'){
        this.add.text(32,15,"Lab 1")
    }
    if(mapn==='map2'){
        this.add.text(32,15,"Lab 2")
    }
    if(mapn==='map3'){
        this.add.text(32,15,"Lab 3")
    }
    if(mapn==='map4'){
        this.add.text(32,15,"Lab 4")
    }

    //timer
    timeText = this.add.text(32,32)
    timeEvent = this.time.addEvent({
        delay : 1000,
        callback : onEvent,
        callbackScope:this,
        repeat :60
    })

    //create animations
    cursors = this.input.keyboard.createCursorKeys();

     this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
        frameRate: 1,
        repeat: -1
    });

    this.anims.create({
        key: 'turn',
        frames: [ { key: 'dude', frame: 4 } ],
        frameRate: 2
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
        frameRate: 1,
        repeat: -1
    });


}


function update ()
{
    //fog of war follow player
    spotlight.x = player.x;
    spotlight.y = player.y;

    //change physics if special tile hit
    if(!ice){
        player.body.setVelocity(0);
    }else{
        speed=450;
    }

    //movement
    if (cursors.left.isDown)
    {
        player.body.setVelocityX(-speed);

        player.anims.play('left', true);
    }
    else if (cursors.right.isDown)
    {
        player.body.setVelocityX(speed);

        player.anims.play('right', true);
    }
    else if (cursors.down.isDown)
    {
        player.body.setVelocityY(speed);;

    }
    else if (cursors.up.isDown)
    {
        player.body.setVelocityY(-speed);;

    }
    else
    {
        player.anims.play('turn');
    }

    //timer text
    timeText.setText('time : ' + counter);

    var tile = layerUP.getTileAtWorldXY(player.x,player.y,true);
    
    //end of game counte hit0 or player hit final tile
    if(tile.index===43 || counter===0){ 
        endGame = true;
        if(tile.index===43){
            player.x = 0;
            player.y = 0;
            var fin = this.add.image(500,400,'fin');
            fin.setScale(0.5)
            var img = this.add.image(350, 500,"scores").setInteractive();
            img.setScale(0.5,0.5),
            img.once('pointerup', showscore, this);
            this.add.text(32,64,"Tu commences doucement a ouvrir les yeux, tu regardes autour de toi");
            this.add.text(32,96,"et apercois le reste de ta classe. Tu tournes la tete car tu entends ");
            this.add.text(32,128,"Jules appeler,il est dans le lit a cote de toi. Tu te rend alors compte");
            this.add.text(32,160,"que vous avez reussis.Tout le monde se reveil. Vos parents attende dans la");
            this.add.text(32,192,"salle d attente de l hopital.Tu es heureux mais cette histoire avec Marie");
            this.add.text(32,224,"te chiffonne encore. Tu te promets alors de faire plus attention aux gens" );
            this.add.text(32,256,"qui proche car personne ne merite ce qui est arrivee a Marie.");
            this.add.text(32,288,"Tu sors de cette histoire grandi.")
            this.add.text(15,500,"Voir votre score final")
        }
        if(counter===0){
            this.add.text(15,500,"Vous avez echoue...")
            var img = this.add.image(600, 500,"scores").setInteractive();
            img.setScale(0.5,0.5),
            img.once('pointerup', showscore, this);
        }
        
    }

    //special effect according to wich tiles are hit
    if(tile.index===59){ 
        this.add.text(300,20,"Wouaw le sol est devenue de la glace !!")
        ice=true;
        }
    if(tile.index===48){ 
        this.add.text(300,20,"Vous etes tombe dans de la glue.. !!")
        speed=50;
        }
    if(tile.index===129){ 
        this.add.text(300,20,"Vous glissez sur du sang..!!")
        player.body.setVelocityX(-speed);

        player.anims.play('left', true);
        }
        
    

        

}

//add final score and displayall the scores
function showscore(){
    
    postScore(counter);
    setInterval(getScores,1000);
    $("#scores").show();
    $("#reset").show();
    

}


function onEvent(){
    if (!endGame){
    counter--;
}


}



