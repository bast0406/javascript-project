
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;

@SuppressWarnings("serial")
public class ScoresServlet extends HttpServlet {
	// this is our GET /users API providing all the scores as a JSON object
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{ 
		try {
			// the scores DB is a json file
			// read the file
			// get the content of the file to be sent back to the client
			String json = "{\"success\":\"true\", \"data\":" ;
			json += new String(Files.readAllBytes(Paths.get("./data/scores.json")));
			json += "}";
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			resp.setStatus(HttpServletResponse.SC_OK); 
			resp.getWriter().write(json);			
		}

		catch(Exception e) {
			e.printStackTrace();
			String json = "{\"success\":\"false\", \"error\":" ;
			json += e.getMessage();
			json += "}";
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); 
			resp.getWriter().write(json);
		}
	}

	// this is our POST /users API providing all the scores as a JSON object
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			// get the parameters sent by the POST form
			String pseudo = req.getParameter("pseudo");
			String score = req.getParameter("score");
			int scoreInt = Integer.parseInt(score);
			boolean dejaPresent = false;


			// read the file
			// get the content of the file
			String json = new String(Files.readAllBytes(Paths.get("./data/scores.json")));
			// get the JSON object from the string (content of the file): deserialized to a
			// list 
			Genson genson = new Genson();
			List<Score> users = genson.deserialize(json, new GenericType<List<Score>>() {
			});


			//check if the pseudo is already in the JSON and update the score
			for (Score user : users) {
				if (user.pseudo.equals(pseudo)) {
					dejaPresent = true;
					System.out.println("Update users: " + user.pseudo);


					//reset the score
					if(scoreInt==0) {
						user.score=0;
					}else {
						user.score += scoreInt;
					}


				}else {
				}
			}
			//if not in the JSON, add it
			if(!dejaPresent) {
				users.add(new Score(pseudo,scoreInt));
			}

			json = genson.serialize(users);
			Files.write(Paths.get("./data/scores.json"), json.getBytes());

			// send simply a success info
			json = "{\"success\":\"true\"}";
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write(json);
		}

		catch (Exception e) {
			e.printStackTrace();
			String json = "{\"success\":\"false\", \"error\":";
			json += e.getMessage();
			json += "}";
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resp.getWriter().write(json);
		}
	}
}


class Score {
	public String pseudo ;
	public int score;
	
	public Score() {};

	public Score(String pseudo, int score){
		this.pseudo = pseudo;
		this.score=score;
	}



}
